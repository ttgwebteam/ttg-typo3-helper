## TTG TYPO3 Helper

1. **Downloads** auf der linken Seite anklicken und das Repository herunterladen
2. Zip File entpacken
3. Im Chrome auf die 3 Punkte klicken und "Weitere Tools" => "Erweiterungen" anklicken
4. Entwicklermodus aktivieren
5. Entpackte Erweiterung laden anklicken
6. den Ordner auswählen, wo die Erweiterung "unzipped" wurde

Bei fast allen TTG Webprojekten sieht man, wenn man mit der Maus über das Logo fährt dann in der linken oberen Ecke die aktuelle Page-ID und sofern vorhanden auch die ID der Weltkugel.

![HowTo](howto.png)
